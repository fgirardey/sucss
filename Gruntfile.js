module.exports = function(grunt) {

	// Project configuration.
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		watch: {
			css: {
				files: ['assets/**/*.scss'],
				tasks: ['sass'],
			}
		},
		sass: {
			dist: {
				options: {
					style: 'expanded',
					noCache: true
				},
				files: [{
					expand: true,
					cwd: 'assets/scss/',
					src: ['*.scss'],
					dest: 'assets/css/',
					ext: '.css'
				}]
			}
		}
	});

	// Load the plugin that provides the "uglify" task.
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-sass');

	// Default task(s).
	grunt.registerTask('default', ['watch']);

};